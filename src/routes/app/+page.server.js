export const load = async ({ request }) => {
    const userAgent = request.headers.get('user-agent')?.toLowerCase() || '';
    
    // Проверяем, является ли устройство Android
    const isAndroid = /android/.test(userAgent);
    
    return {
      isAndroid
    };
  };