// import { createCanvas, loadImage, registerFont } from 'canvas';

import { createCanvas, GlobalFonts, loadImage } from '@napi-rs/canvas'

async function generateResultImage(pastImage, currentImage, futureImage, backgroundUrl) {
	const canvasWidth = 1200;
	const canvasHeight = 630;
	// registerFont('static/fonts/philosopher.regular.ttf', { family: 'philosopher' })
	GlobalFonts.registerFromPath('./philosopher.regular.ttf', 'philosopher')

	const canvas = createCanvas(canvasWidth, canvasHeight);
	const ctx = canvas.getContext('2d');

	// Load and draw the background image
	const backgroundImage = await loadImage(backgroundUrl);
	ctx.drawImage(backgroundImage, 0, -250, 1200, 1200);

	ctx.fillStyle = 'rgba(0, 0, 0, 0.3)';
	ctx.fillRect(0, 0, canvasWidth, canvasHeight);

	ctx.translate(50, 150);
	ctx.rotate(-0.5235987756);
	const pastImageLoaded = await loadImage(pastImage);
	ctx.drawImage(pastImageLoaded, 0, 0, 900, 1575, 0, 0, 225 * 1.3, 393 * 1.3);

	ctx.translate(800, 290);
	ctx.rotate(0.5235987756 * 2);
	const futureImageLoaded = await loadImage(futureImage);
	ctx.drawImage(futureImageLoaded, 0, 0, 900, 1575, 0, 0, 225 * 1.3, 393 * 1.3);

	ctx.translate(-350, 250);
	ctx.rotate(-0.5235987756);
	const currentImageLoaded = await loadImage(currentImage);
	ctx.drawImage(currentImageLoaded, 0, 0, 900, 1575, 0, 0, 225 * 1.3, 393 * 1.3);

	ctx.rotate(-0.5235987756);
	const gradient = ctx.createLinearGradient(0, 0, 1000, 0);
	ctx.rotate(0.5235987756);
	ctx.translate(-360, 380);
	gradient.addColorStop(0, 'transparent');
	gradient.addColorStop(0.0961, 'rgba(254, 244, 222, 0.7)');
	gradient.addColorStop(0.4978, 'rgba(254, 244, 222, 0.7)');
	gradient.addColorStop(0.9298, 'rgba(254, 244, 222, 0.7)');
	gradient.addColorStop(1, 'transparent');

	// await document.fonts.load('10pt "philosopher"');
	
	ctx.fillStyle = gradient;
	ctx.fillRect(0, 0, 1200, 100);
	ctx.fillStyle = '#010030';
	ctx.font = '80px philosopher';
	ctx.fillText('ГАДАНИЕ ТАРО', 195, 80);

	// Save the image to a file
	const buffer = await canvas.encode('png')
	return buffer;
	// fs.writeFileSync('result.png', buffer);
}

export async function GET(data) {
	const params = data;
	const { origin, searchParams } = params.url;
	const past = searchParams.get('p');
	const current = searchParams.get('c');
	const future = searchParams.get('f');

	const pastImage = past ? `${origin}/img/card/cats/${past}.jpg` : null;
	const currentImage = current ? `${origin}/img/card/cats/${current}.jpg` : null;
	const futureImage = future ? `${origin}/img/card/cats/${future}.jpg` : null;

	const backgroundUrl = `${origin}/img/bg3.jpg`;

	// const gameName = 'Dice Quest';

	const imageData = await generateResultImage(pastImage, currentImage, futureImage, backgroundUrl);
	const image = Buffer.from(imageData, 'base64');

	return new Response(image, {
		status: 200,
		headers: {
			'Content-Type': 'image/png',
			'Content-Length': image.length,
			'Content-Disposition':
				// Use filename* instead of filename to support non-ASCII characters
				`attachment; filename*=UTF-8''image.png`
		}
	});
}
