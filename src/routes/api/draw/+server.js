export async function POST({ request }) {
	const data = await request.json();
	const {API_KEY, FOLDER_ID, TELEGRAM_BOT_TOKEN, CHAT_ID } = process.env

	const myHeaders = new Headers();
	myHeaders.append('Content-Type', 'application/json');
	myHeaders.append('Authorization', `Api-Key ${API_KEY}`);
	myHeaders.append('x-folder-id', FOLDER_ID);
	const draw = `прошлое - ${data.layout[0]?.name}${
		data.layout[0]?.orientation ? ' перевернута' : ''
	}, настоящее - ${data.layout[1]?.name}${
		data.layout[1]?.orientation ? ' перевернута' : ''
	}, будущее - ${data.layout[2]?.name}${data.layout[2]?.orientation ? ' перевернута' : ''}`;

	//log questions
	try {
		fetch(`https://api.telegram.org/bot${TELEGRAM_BOT_TOKEN}/sendMessage`, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({ chat_id: CHAT_ID, text: 'Вопрос: ' + data.question })
		});
	} catch(err){/* */}

	const raw = JSON.stringify({
		modelUri: 'gpt://b1g4v9h4g2s1bp734qkg/yandexgpt/latest',
		completionOptions: {
			stream: false,
			temperature: 0.75,
			maxTokens: '1500'
		},
		messages: [
			{
				role: 'system',
				text: `Ты - таролог. Расклад прошлое, настоящее и будущее.
				В ответе должно быть только толкование карт и ничего, кроме толкования
				Формат ответа:
				1. Толкование карты прошлого
				2. Толкование карты настоящего
				3. Толкование карты будущего
				`
			},
			{
				role: 'user',
				text: `Расклад ${draw}. Вопрос: ${data.question}`
			}
		]
	});
	
	const requestOptions = {
		method: 'POST',
		headers: myHeaders,
		body: raw,
		redirect: 'follow'
	};

	try {
		const res = await fetch(
			'https://llm.api.cloud.yandex.net/foundationModels/v1/completionAsync',
			requestOptions
		);
		const json = await res.json();
		return new Response(`{"id": "${json.id}"}`);
	} catch (err) {
		return new Error(err);
	}
}
