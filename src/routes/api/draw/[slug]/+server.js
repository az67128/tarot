import { kv } from '@vercel/kv';

export async function GET({ params }) {
	const {API_KEY, FOLDER_ID } = process.env

	// const data = await request.json();

	// const {TELEGRAM_BOT_TOKEN, CHAT_ID } = process.env

	try {
		let cache;
		try {
			cache = await kv.get(params.slug);
		} catch (err) {
			//nothing
		}

		let json;
		if (cache) {
			json = cache;
		} else {
			const res = await fetch(`https://llm.api.cloud.yandex.net/operations/${params.slug}`, {
				headers: { Authorization: `Api-Key ${API_KEY}` }
			});

			json = await res.json();
			try{
			if (json.done) {
				await kv.set(params.slug, JSON.stringify(json), { ex: 60 * 60 * 24 * 30, nx: true });
			}
		} catch(e){}
		}
		return new Response(JSON.stringify(json));
	} catch (err) {
		return new Response(err);
	}
}
