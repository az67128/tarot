export async function POST({ request }) {
	
	const data = await request.json();
	const {MISTRAL_API_KEY, FOLDER_ID, TELEGRAM_BOT_TOKEN, CHAT_ID } = process.env

	const myHeaders = new Headers();
	myHeaders.append('Content-Type', 'application/json');
	myHeaders.append('Authorization', `Bearer ${MISTRAL_API_KEY}`);
	
	const draw = `прошлое - ${data.layout[0]?.name}${
		data.layout[0]?.orientation ? ' перевернута' : ''
	}, настоящее - ${data.layout[1]?.name}${
		data.layout[1]?.orientation ? ' перевернута' : ''
	}, будущее - ${data.layout[2]?.name}${data.layout[2]?.orientation ? ' перевернута' : ''}`;
	
	//log questions
	try {
		fetch(`https://api.telegram.org/bot${TELEGRAM_BOT_TOKEN}/sendMessage`, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({ chat_id: CHAT_ID, text: 'Вопрос: ' + data.question })
		});
	} catch(err){/* */}

	const raw = JSON.stringify({
		"model": "pixtral-12b-2409",
		
		messages: [
			{
				role: 'system',
				content: `Ты - таролог. Расклад прошлое, настоящее и будущее.
				В ответе должно быть только толкование карт и ничего, кроме толкования
				Формат ответа:
				1. Толкование карты прошлого
				2. Толкование карты настоящего
				3. Толкование карты будущего
				Общий вывод
				`
			},
			{
				role: 'user',
				content: `Расклад ${draw}. Вопрос: ${data.question}`
			}
		]
	});
	
	const requestOptions = {
		method: 'POST',
		headers: myHeaders,
		body: raw,
		redirect: 'follow'
	};
	console.log('test111', raw)
	try {
		const res = await fetch(
			'https://api.mistral.ai/v1/chat/completions',
			requestOptions
		);
		const json = await res.json();
		console.log(json)
		return new Response(JSON.stringify(json));
	} catch (err) {
		return new Error(err);
	}
}
