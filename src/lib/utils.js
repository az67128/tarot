const IS_ENABLED = false;
export function pickThreeUnique(arr) {
	let result = [];
	let copyArr = [...arr];

	for (let i = 0; i < 3; i++) {
		let randomIndex = Math.floor(Math.random() * copyArr.length);
		result.push({ ...copyArr[randomIndex], orientation: Math.random() > 0.8 ? 1 : 0 });
		fetch(`/img/card${copyArr[randomIndex].img}`);
		copyArr.splice(randomIndex, 1);
	}
	fetch(`/img/card/back.png`);
	return result;
}

export function createFloorAd() {
	if(!IS_ENABLED) return ()=>{}
	window?.yaContextCb.push(() => {
		if (window?.Ya?.Context?.AdvManager.getPlatform() === 'desktop') {
			window?.Ya?.Context?.AdvManager.render({
				blockId: 'R-A-4908148-3',
				type: 'floorAd',
				platform: 'desktop'
			});
		} else {
			window?.Ya?.Context?.AdvManager.render({
				blockId: 'R-A-4908148-1',
				type: 'floorAd',
				platform: 'touch'
			});
		}
	});
	return () => {
		window?.Ya?.Context?.AdvManager.destroy({
			blockId: 'R-A-4908148-3'
		});
		window?.Ya?.Context?.AdvManager.destroy({
			blockId: 'R-A-4908148-1'
		});
	};
}

export function createFoolScreenAd() {
	if(!IS_ENABLED) return ()=>{}
	window?.yaContextCb.push(() => {
		if (window?.Ya?.Context?.AdvManager.getPlatform() === 'desktop') {
			window?.Ya.Context?.AdvManager.render({
				blockId: 'R-A-4908148-5',
				type: 'fullscreen',
				platform: 'desktop'
			});
		} else {
			window?.Ya.Context?.AdvManager.render({
				blockId: 'R-A-4908148-4',
				type: 'fullscreen',
				platform: 'touch'
			});
		}
	});
}
